import React, {useState} from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, View, TextInput, TouchableOpacity, Pressable } from "react-native";
import { MaterialIcons } from '@expo/vector-icons'; 

export default TaskInputField = (props) => {
    const [task, setTask] = useState();

    const handleAddTask = (value) => {
        props.addTask(value);
        setTask(null);
    }

    return (
        <KeyboardAvoidingView 
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <TextInput 
            style={styles.inputField} 
            value={task} 
            onChangeText={text => setTask(text)} 
            onSubmitEditing={() => handleAddTask(task)}
            placeholder={'What do we require?'} 
            placeholderTextColor={'#C29A9A'}/>
       
        
          {/* <View style={styles.button}>
              <MaterialIcons name="keyboard-arrow-down" size={24} color="black" />
          </View> */}

      </KeyboardAvoidingView>
    );
}

/* Main Field: 22 
  #C29A9A */

const styles = StyleSheet.create({
    container: {
        borderColor: '#fff',
        borderBottomWidth: 3,
        borderBottomColor: '#A88888',
        marginHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        position: 'absolute',
        bottom: 20,
    },
    inputField: {
        color: '#C29A9A',
        height: 40,
        flex: 1,
        textAlign: 'center',
        fontSize: 22,
        fontFamily: 'NoticiaTextItalic',
    },
    button: {
        height: 30,
        width: 30,
        borderRadius: 5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
});