import React, { forwardRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Pressable } from "react-native";
import { MaterialIcons } from '@expo/vector-icons';
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { LinearGradient } from 'expo-linear-gradient';

export default TaskItem = (props) => {

    const completeOrPending = function () {
        return props.isComplete ? styles.complete : null;
    }

    const styleVariations = [
        {
            name: 'red',
            colors: {
                gradient1: 'rgba(246,71,71,1)',
                gradient2: 'rgba(246,71,71,0)',
                main: '#C32C2C',
                cross: '#A11616',
                font: '#FF6060'
            },
            offsetX: 0
        },
        {
            name: 'grey',
            colors: {
                gradient1: 'rgba(164,164,164,1)',
                gradient2: 'rgba(164,164,164,0)',
                main: '#898989',
                cross: '#898989',
                font: '#4A4A4A'
            },
            offsetX: 10
        },
        {
            name: 'blue',
            colors: {
                gradient1: 'rgba(95,178,255,1)',
                gradient2: 'rgba(95,178,255,0)',
                main: '#1A427E',
                cross: '#1A427E',
                font: '#70C4F3'
            },
            offsetX: -10
        },
        {
            name: 'yellow',
            colors: {
                gradient1: 'rgba(249,232,85,1)',
                gradient2: 'rgba(249,232,85,0)',
                main: '#DFC32E',
                cross: '#DFC32E',
                font: '#9A6E2D'
            },
            offsetX: 17
        }
    ];

    const currentStyleVar = function () {
        let varIndex = props.index % styleVariations.length;
        return styleVariations[varIndex];
    }

    const isOffset = function (index) {
        if (index > 0) {
            //return styles.verticalOffset;
            return {
                transform: [{translateY: -85*index}],
            }
        } else {
            return null;
        }
    }

    const RightActions = () => {
        return (
            <View
                style={{ 
                    width: 228,
                    alignItems: 'flex-end' 
                }}>
            </View>
        )
    }

    return (

        <View style={[
            styles.container, 
            {zIndex: props.index*-1, elevation: props.index*-1},
            isOffset(props.index) 
            ]}>

            {/* <Pressable
                style={[styles.taskContainer, completeOrPending()]}
                onPress={() => props.completeTask(props.index)}
            >
                <Text style={styles.task}>{props.task}</Text>
                <TouchableOpacity onPress={() => props.deleteTask(props.index)}>
                    <MaterialIcons style={styles.delete} name="delete" size={18} color='#fff' />
                </TouchableOpacity>
            </Pressable> */}

            <Swipeable 
                renderRightActions={RightActions}
                containerStyle={[
                    styles.swipeContainer
                ]}
                overshootLeft={100}
                overshootRight={100}
                rightThreshold={100}
            >

                <View style={[
                    styles.taskContainer,
                    {
                        backgroundColor: currentStyleVar().colors.main,
                        transform: [{translateX: currentStyleVar().offsetX}]
                    } 
                ]}>
                    <View style={styles.taskBody}>
                        <Text style={[
                            styles.taskText, 
                            {color: currentStyleVar().colors.font} 
                        ]}>{props.task}</Text>
                    </View>
                    <Pressable 
                        style={styles.taskSide}
                        onPress={() => props.deleteTask(props.index)}
                    >
                        <View style={styles.taskCross}>
                            <View style={[
                                styles.cross, 
                                styles.crossDown,
                                {borderLeftColor: currentStyleVar().colors.cross}
                            ]}></View>
                            <View style={[
                                styles.cross, 
                                styles.crossUp,
                                {borderLeftColor: currentStyleVar().colors.cross}
                            ]}></View>
                        </View>
                    </Pressable>
                    <LinearGradient
                        // Background Linear Gradient
                        colors={[currentStyleVar().colors.gradient1, currentStyleVar().colors.gradient2]}
                        start={{ x: 0.1, y: 1.3 }}
                        end={{ x: 0.6, y: -0.4 }}
                        style={styles.taskLid}
                    >
                    </LinearGradient>
                </View>
                

            </Swipeable>

        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    swipeContainer: {
        flex: 1,
    },
    // verticalOffset: {
    //     transform: [{ translateY: -80 }],
    // },
    indexContainer: {
        backgroundColor: '#3E3364',
        borderRadius: 12,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
    },
    index: {
        color: '#fff',
        fontSize: 13,
    },
    taskContainer: {
        borderRadius: 23,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        flex: 1,
        height: 150,
        marginLeft: 40,
        marginRight: 40,
        // transform: [{ translateY: -50 }]
    },
    taskLid: {
        position: 'absolute',
        top: 0,
        width: '100%',
        height: 100,
        borderRadius: 23,
    },
    taskBody: {
        position: 'absolute',
        bottom: 10,
        left: 20,

    },
    taskSide: {
        width: 65,
        height: '100%',
        position: 'absolute',
        right: 0,
        borderRadius: 23,
        borderBottomLeftRadius: 0,
        borderTopLeftRadius: 0,
        backgroundColor: 'rgba(0,0,0,0.15)'
    },
    taskCross: {
        position: 'absolute',
        bottom: 1,
        left: 0,
    },
    cross: {
        width: 10,
        height: 23,
        position: 'absolute',
        borderLeftWidth: 4,
        left: 25,
    },
    crossDown: {
        bottom: 10,
        transform: [{ rotate: "45deg" }]
    },
    crossUp: {
        bottom: 14,
        transform: [{ rotate: "-45deg" }]
    },
    taskText: {
        fontSize: 23,
        marginLeft: 8,
        fontFamily: 'NoticiaTextBold',
    },
    taskText1: {
        color: '#FF6060',
    },
    complete: {
        backgroundColor: 'pink'
    },
    task: {
        color: '#fff',
        width: '90%',
        fontSize: 16,
    },
    delete: {
        marginLeft: 10,
    },
});