import React, { useState, useEffect } from 'react';
import { Keyboard, ScrollView, StyleSheet, Text, View, Pressable } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { useFonts } from 'expo-font';
import TaskInputField from './components/TaskInputField';
import TaskItem from './components/TaskItem';
//import { toWords } from './helpers/toWords';
import { REACT_APP_DEEPL_API_KEY } from "@env"
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { Path, Circle, Line } from "react-native-svg";

export default function App() {
  const [tasks, setTasks] = useState([]);
  const [isTranslated, setIsTranslated] = useState(false);

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: 'Deutsch', value: 'de' },
    { label: 'Italian', value: 'it' }
  ]);

  const [fontsLoaded] = useFonts({
    NoticiaTextBold: require('./assets/fonts/NoticiaText-Bold.ttf'),
    NoticiaTextBoldItalic: require('./assets/fonts/NoticiaText-BoldItalic.ttf'),
    NoticiaTextItalic: require('./assets/fonts/NoticiaText-Italic.ttf'),
    NoticiaTextRegular: require('./assets/fonts/NoticiaText-Regular.ttf'),
  });

  const addTask = (task) => {
    if (task == null) return;
    setTasks([...tasks, {
      text: task,
      isComplete: false
    }]);
    //Keyboard.dismiss();
  }

  useEffect(() => {
    // load the tasks from the phone memory 
    getData();
  }, []);

  useEffect(() => {
    storeData(tasks);
  }, [tasks]);

  const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem('@tasks', jsonValue)
    } catch (e) {
      // saving error
      console.log(e);
    }
  }

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@tasks');
      if (value !== null) {
        setTasks(JSON.parse(value));
      }
    } catch (e) {
      console.log(e);
      // error reading value
    }
  }



  const deleteTask = (deleteIndex) => {
    setTasks(tasks.filter((value, index) => index != deleteIndex));
  }

  const completeTask = (updateIndex) => {

    // update the tasks array 

    // need to create a new array as arrays in state are immutable 
    const updatedTasks = tasks.map((task, index) => {
      if (index == updateIndex) {

        // if the task is not complete 
        if (tasks[index].isComplete == false) {
          // complete it 
          return {
            ...task,
            isComplete: true,
          };
          // if the task is complete 
        } else {
          // uncheck it
          return {
            ...task,
            isComplete: false,
          };
        }
      } else {
        return task;
      }
    });

    setTasks(updatedTasks);

  }

  const translateTasks = async (language) => {

    if (tasks.length == 0) return;

    // not having this allows to re-translate the tasks once the new have been added
    //if (tasks[0].translationLang == language) return;

    let tasksString = "";

    for (let index = 0; index < tasks.length; index++) {
      let divider = (index == tasks.length - 1) ? "" : ";";
      tasksString += tasks[index].text + divider;
    }

    const res = await fetch("https://api-free.deepl.com/v2/translate?" + new URLSearchParams({
      text: tasksString,
      target_lang: language
    }), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": REACT_APP_DEEPL_API_KEY
      }
    });

    const deeplData = await res.json();

    const translationArray = deeplData.translations[0].text.split(";");

    //console.log(translationArray);

    const translatedTasks = tasks.map((task, index) => {
      return {
        ...task,
        translation: translationArray[index],
        translationLang: language
      };
    });

    setTasks(translatedTasks);
    setIsTranslated(true);
  }

  const flipLanguage = () => {
    if (!tasks[0] || !tasks[0].translationLang) return;

    if (isTranslated) {
      setIsTranslated(false);
    } else {
      setIsTranslated(true);
    }
  }

  const flipLanguageButton = () => {
    if (!tasks[0] || !tasks[0].translationLang) return null;

    return <Pressable
      style={styles.hideTranslation}
      onPress={() => flipLanguage()}
    >
      {eyeIcon()}
    </Pressable>
  }

  const eyeIcon = () => {
    if (isTranslated) {
      return <Svg style={styles.eyeIcon} width="25" height="18" viewBox="0 0 25 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Path d="M2.69142 8.99996C2.71015 9.0577 2.72952 9.11523 2.74952 9.17254M6.34866 13.7118C4.6387 12.5688 3.38042 10.9803 2.74952 9.17254M2.74952 9.17254L2.69141 8.93952C3.31591 7.06417 4.61234 5.41867 6.38577 4.25039C8.15921 3.0821 10.3138 2.45417 12.5257 2.46099C14.0842 2.4658 15.6117 2.78555 16.9953 3.38651M18.4017 4.12654C18.4857 4.17926 18.569 4.23315 18.6513 4.28823C20.4155 5.46742 21.6989 7.12089 22.3086 9.00004L22.3086 9.06048C21.6841 10.9358 20.3877 12.5813 18.6142 13.7496C16.8408 14.9179 14.6862 15.5458 12.4743 15.539C10.8895 15.5341 9.33672 15.2036 7.93474 14.5828" stroke="#48A6BB" />
        <Path d="M12.7054 12.338C14.4617 12.338 15.8854 10.9142 15.8854 9.15798C15.8854 8.50332 15.6876 7.89487 15.3485 7.38914L10.9277 11.7951C11.4352 12.1378 12.0469 12.338 12.7054 12.338Z" fill="#90DBEB" />
        <Path d="M12.7054 5.97797C10.9491 5.97797 9.52539 7.40171 9.52539 9.15798C9.52539 9.72796 9.67535 10.2629 9.93797 10.7256L14.2617 6.38416C13.8016 6.1255 13.2708 5.97797 12.7054 5.97797Z" fill="#90DBEB" />
        <Line x1="4.12106" y1="16.2497" x2="19.7775" y2="0.593277" stroke="#90DAEB" />
      </Svg>
    } else {
      return <Svg style={styles.eyeIcon} width="25" height="18" viewBox="0 0 25 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Path d="M6.1929 13.6282C4.45258 12.465 3.18116 10.8576 2.54174 9.04121L2.52087 8.95754C3.16796 7.12038 4.46105 5.49835 6.22985 4.33313C8.04267 3.13889 10.2522 2.49304 12.526 2.50006C14.7998 2.50707 17.004 3.16654 18.8071 4.37178C20.5704 5.55038 21.8522 7.1848 22.4832 9.03078C21.8375 10.8727 20.5427 12.4992 18.7702 13.6669C16.9573 14.8611 14.7478 15.507 12.474 15.4999C10.2002 15.4929 7.99604 14.8335 6.1929 13.6282Z" stroke="#48A6BB" />
        <Circle cx="12.5394" cy="9.18264" r="3.18001" fill="#90DAEB" />
      </Svg>
    }
  }

  const taskText = (task) => {
    return isTranslated ? task.translation : task.text;
  }


  if (!fontsLoaded) {
    return null;
  }

  return (
    <View style={styles.container}>

      <View style={styles.iosTopBar}></View>

      <ScrollView style={styles.scrollView}>
        { //tasks.slice(0).reverse().map((task, index)
          tasks.map((task, index) => {
            return (
              <TaskItem
                key={index}
                index={index}
                //indexInWords={toWords(index+1)}
                task={taskText(task)}
                // ref={refArr.current[index]}
                deleteTask={() => deleteTask(index)}
                completeTask={() => completeTask(index)}
                isComplete={task.isComplete}
              />
            );
          })
        }
      </ScrollView>

      <View style={styles.languageContainer}>
        <DropDownPicker
          style={styles.languageDropdown}
          placeholder={"Language"}
          textStyle={{
            fontSize: 18,
            color: '#C9AE4D',
            fontFamily: 'NoticiaTextRegular',
          }}
          open={open}
          onChangeValue={() => translateTasks(value)}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
        />

      </View>

      <TaskInputField addTask={addTask} />

      {flipLanguageButton()}
    </View>

  );



}

const styles = StyleSheet.create({
  iosTopBar: {
    width: '100%',
    position: 'absolute',
    height: 24,
    backgroundColor: '#5E4545',
  },
  container: {
    flex: 1,
    backgroundColor: '#514343',
  },
  languageContainer: {
    position: 'absolute',
    width: '85%',
    left: 40,
    // marginLeft: 'auto',
    // marginRight: 'auto',
  },
  languageDropdown: {
    width: 145,
    borderRadius: 0,
    paddingTop: 50,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#664B4B',
    borderWidth: 0,
    justifyContent: 'center',
  },
  hideTranslation: {
    position: 'absolute',
    left: 185,
    width: 55,
    height: 85,
    backgroundColor: '#715555',
    alignItems: 'center',
  },
  eyeIcon: {
    marginTop: 54
  },
  heading: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 20,
  },
  scrollView: {
    marginTop: 100,
    marginBottom: 70,
  },
});